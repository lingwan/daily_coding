# Given an array of integers and a number k, where 1<=k<=array length, compute the maximum values of each subarray of
# length k.
# eg: array is [10,5,2,7,8,7] and k=3. We should get [10,7,8,8], since: 10=max(10,5,2),7=max(5,2,7), 8=max(2,7,8), 8=max(7,8,7)

from collections import deque

def max_of_subarrays(lst,k):
    q=deque()
    for i in range(k):
        while q and lst[i]>=lst[q[-1]]:
            q.pop()
        q.append(i)

    # Loop invariant: q is a list of indics where their corresponding values are in descending order.
    for i in range(k,len(lst)):
        print(lst[q[0]])
        while q and q[0]<=i-k:
            q.popleft()
        while q and lst[i]>=lst[q[-1]]:
            q.pop()
        q.append(i)
    print(lst[q[0]])

print(max_of_subarrays([10,5,2,7,8,7],3))
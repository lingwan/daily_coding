# Given a string s and an integer k, break up the string into multiple lines such that each line has a length of k or less.
# You must break it up so that words don't break across lines. Each line has to have the maximum possible amount of words.
# If there's no way to break the text up, then return null.

def breakWords(s,k):
    words=s.split()
    if not words:
        return []
    current=[]
    all=[]

    for i,word in enumerate(words):
        if length(current+[word])<=k:
            current.append(word)
        elif length([word])>k:
            return None
        else:
            all.append(current)
            current=[word]
    all.append(current)

    return all

def length(words):
    if not words:
        return 0
    return sum(len(word) for word in words)+(len(words)-1)
    


print(breakWords("the quick brown fox jumps over the lazy dog",10))


# Suppose you have a multiplication table that is N by N. That is, a 2D array where the value at the i-th row and j-th column is (i + 1) * (j + 1) (if 0-indexed) or i * j (if 1-indexed).
#
# Given integers N and X, write a function that returns the number of times X appears as a value in an N by N multiplication table.
# Solution 1:
def appearTimes(N,X):
    count=0
    for i in range(N):
        for j in range(N):
            if ((i+1)*(j+1)==X):
                count+=1
    return count

print(appearTimes(6,12))

# Solution 2:
def multi_tables(n,x):
    count=0
    for i in range(1,n+1):
        if x%i==0 and x/i<=n:
            count+=1
    return count

# Given a linked list rearrange the node values such that they appeare in alternating low->high->low->high->...form
# eg: given 1->2->3->4->5, you should return 1->3->2->5->4

# Check every other node
# If the previous nodes's value is greater, swap the current and previous values.
# If the next nodes's value is greater, swap the current and next values.
# Use two pointers that jump forward two steps after each check.

def alternate(ll):
    prev=ll
    cur=ll.next

    while cur:
        if prev.data>cur.data:
            prev.data,cur.data=cur.data,prev.data

        if not cur.next:
            break

        if cur.next.data>cur.data:
            cur.next.data,cur.data=cur.data,cur.next.data

        prev=cur.next
        cur=cur.next.next
        
    return ll
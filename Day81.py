# Given two strings A and B, return whether or not A can be shifted some number of times to get B.
# For example, if A is abcde and B is cdeab, return true. If A is abc and B is acb, return false.
# Create another String by concatenating first String with itself, now check if second String is a substring of this
# concatenated String or not, if yes, the second String is a rotation of first.

def isShifted(str1,str2):
    if (len(str1)!=len(str2)):
        return False
    str=str1+str1
    if str2 in str:
        return True
    else:
        return False

print(isShifted("abcde","cdeab"))
print(isShifted("abc","acb"))


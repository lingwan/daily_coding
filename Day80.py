# Print the nodes in a binary tree level-wise. For example, the following should print 1, 2, 3, 4, 5.
#
#   1
#  / \
# 2   3
#    / \
#   4   5
class Node:
    # A utility function to create a new node
    def __init__(self ,key):
        self.data = key
        self.left = None
        self.right = None

def printLevelOrder(root):
    # Base case
    if root is None:
        return

    queue=[]
    #Enqueue Root
    queue.append(root)

    while(len(queue)>0):
        # Print fron of queue and remove it from queue
        print(queue[0].data)
        node=queue.pop(0)

        #Enqueue left child
        if node.left is not None:
            queue.append(node.left)
        # Enqueue right child
            queue.append(node.right)

root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)

printLevelOrder(root)
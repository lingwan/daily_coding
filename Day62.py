# Given a string of parentheses, write a function to compute the minimum number of parentheses to be removed to make
# the string valid (i.e. each open parenthesis is eventually closed).
#
# For example, given the string "()())()", you should return 1. Given the string ")(", you should return 2, since we must
# remove all of them.

def minParentheses(p):
    # maintain balance of string
    bal = 0
    ans = 0
    for i in range(0, len(p)):
        if (p[i] == '('):
            bal += 1
        else:
            bal += -1

        # It is guaranteed bal >= -1
        if (bal == -1):
            ans += 1
            bal += 1
    return bal + ans

# print(minParentheses("()))"))

# Solution:
def count_invalid_parenthesis(string):
    opened=0
    invalid=0
    for c in string:
        if c=='(':
            opened+=1
        elif c==')':
            if opened>0:
                opened-=1
            else:
                invalid+=1
    invalid+=opened
    return invalid

print(count_invalid_parenthesis("()())()"))
print(count_invalid_parenthesis(")("))

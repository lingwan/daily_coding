# Determine whether a doubly linked list is a palindrome. What if it’s singly linked?
# For example, 1 -> 4 -> 3 -> 4 -> 1 returns True while 1 -> 4 returns False.

# doubly linked list
def isPalindrome(left):
    if left==None:
        return True
    # Find rightmost node
    right=left
    while right.next!=None:
        right=right.next

    while left!=right:
        if left.data!=right.data:
            return False
        left=left.next
        right=right.prev

    return True

# singly linked list
# check if str is palindrome or not
def isPalindromeUtil(string):
    return string==string[::-1]

# Returns true if string formed by linked list is palindrome
def isPalindrome():
    # Append all nodes to form a string
    temp=[]
    while node is not None:
        temp.append(node.data)
        node=node.next
    string="".join(temp)
    return isPalindromeUtil(string)


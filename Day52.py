# You are given an N by M 2D matrix of lowercase letters. Determine the minimum number of columns that can be removed
# to ensure that each row is ordered from top to bottom lexicographically. That is, the letter at each column is lexicographically later as you go down each row. It does not matter whether each row itself is ordered lexicographically.
# This solution is incorrect when execution

def bad_cols(board):
    num_bad_cols=0
    num_cols=len(board[0])
    i=0
    while i<num_cols:
        if is_sorted_up_to(board,i):
            i+=1
            continue
        else:
            remove_col(board,i)
            num_bad_cols+=1
            num_cols-=1
    return num_bad_cols

def remove_col(board, i):
    for row in board:
        row.pop(i)

def is_sorted_up_to(board,i):
    return all(board[r][:i+1]<=board[r+1][:i+1] for r in range(len(board)-1))


print(bad_cols([["z","y","x"],["w","v","u"],["t","s","r"]]))
# print(bad_cols(["abcdef"]))
# print(bad_cols([["c","b","a"],["d","a","f"],["g","h","i"]]))
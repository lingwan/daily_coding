# Given a string, find the palindrome that can be made by inserting the fewest number of characters as possible anywhere
# in the word. If there is more than one palindrome of minimum length that can be made, return the lexicographically earliest one (the first one alphabetically).

cache = {}

def is_palindrome(s):
    return s == s[::-1]

def make_palindrome(s):
    if s in cache:
        return cache[s]

    if is_palindrome(s):
        cache[s] = s
        return s
    if s[0] == s[-1]:
        result = s[0] + make_palindrome(s[1:-1]) + s[-1]
        cache[s] = result
        return result
    else:
        one = s[0] + make_palindrome(s[1:]) + s[0]
        two = s[-1] + make_palindrome(s[:-1]) + s[-1]
        cache[s] = min(one, two)
        return min(one, two)

print(make_palindrome("race"));
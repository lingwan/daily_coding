# Implement an autocomplete system. That is, given a query string s and a set of all possible query strings, return all strings in the set that have s as a prefix.
# Solution 1 runtime:O(n), n is the number of words in the dictionary

WORDS=['foo','bar','dear','dog','deal']
# def autocomplete(s):
#     results=set()
#     for word in WORDS:
#         if word.startswith(s):
#             results.add(word)
#     return results




# Solution 2 the worst-case runtime is O(n)if all the search results have that prefix,
# if the words are uniformly distributed across the alphabet, it should be much faster on average since we no longer have to evaluate words that don't start with our prefix.

END_HERE='_END_HERE'

class Trie(object):
    def __init__(self):
        self.__trie={}

    def insert(self,text):
        tire=self.__trie
        for char in text:
            if char not in tire:
                tire[char]={}
            tire=tire[char]
        tire[END_HERE]=True

    def elements(self,prefix):
        d=self.__trie
        for char in prefix:
            if char in d:
                d=d[char]
            else:
                return []
        return  self.__elements(d)

    def __elements(self,d):
        result=[]
        for c,v in d.items():
            if c==END_HERE:
                subresult=['']
            else:
                subresult=[c+s for s in self.__elements(v)]
            result.extend(subresult)
        return result

trie=Trie()
for word in WORDS:
    trie.insert(word)

def autocomplete(s):
    suffixes=trie.elements(s)
    return [s+w for w in suffixes]

print(autocomplete('de'))
# You are in an infinite 2D grid where you can move in any of the 8 directions:
#
#  (x,y) to
#     (x+1, y),
#     (x - 1, y),
#     (x, y+1),
#     (x, y-1),
#     (x-1, y-1),
#     (x+1,y+1),
#     (x-1,y+1),
#     (x+1,y-1)
# You are given a sequence of points and the order in which you need to cover the points. Give the minimum number of steps in which you can achieve it. You start from the first point.
#
# Example:
#
# Input: [(0, 0), (1, 1), (1, 2)]
# Output: 2
# It takes 1 step to move from (0, 0) to (1, 1). It takes one more step to move from (1, 1) to (1, 2).

# minimum steps from point1 to points
def shortestPath(point1,point2):
    dx=abs(point1[0]-point2[0])
    dy= abs(point1[1] - point2[1])
    return max(dx,dy)

# return the minimum steps
def coverPoints(sequence):
    stepCount=0
    for i in range(len(sequence)-1):
        stepCount+=shortestPath(sequence[i],sequence[i+1])
    return stepCount

print(coverPoints([(0, 0), (1, 1), (1, 2)]))
print(coverPoints([(4, 6), (1, 2), (4, 5), (10, 12)]))

# There exists a staircase with N steps, and you can climb up either 1 or 2 steps at a time. Given N, write a function that returns the number of unique ways you can climb the staircase. The order of the steps matters.
# [hard]

def staircase(n,X):
    if n<0:
        return 0
    elif n==0:
        return 1
    else:
        return sum(staircase(n-x, X) for x in X)


print(staircase(4,{1,2}))
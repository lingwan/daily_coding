# Implement a URL shortener with the following methods:
#
# shorten(url), which shortens the url into a six-character alphanumeric string, such as zLg6wl.
# restore(short), which expands the shortened string into the original url. If no such shortened string exists, return null.

import random
import string

class URLShortener:
    def __init__(self):
        self.short_to_url={}

    def _generate_short(self):
        return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))

    def _generate_unused_short(self):
        t=self._generate_short()
        while t in self.short_to_url:
            t=self._generate_short()
        return t

    def shorten(self,url):
        short=self._generate_unused_short()
        self.short_to_url[short]=url
        return short

    def restore(self,short):
        return self.short_to_url.get(short,None)
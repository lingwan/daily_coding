# Given a string of words delimited by spaces, reverse the words in string.
# For example, given "hello world here", return "here world hello"

def reverseWords(s):
    words=s.split(' ')
    str=[]
    for word in words:
        str.insert(0,word)
    print(" ".join(str))

reverseWords("hello world here")
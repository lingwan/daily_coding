# Given a number in the form of a list of digits, return all possible permutations.
#
# For example, given [1,2,3], return [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]].

# parameters:
# 1.String
# 2.Starting index of the string
# 3.Ending index of the string
def permute(a,l,r):
    if l==r:
        print(''.join(a))
    else:
        for i in range(l,r+1):
            a[l],a[i]=a[i],a[l]
            permute(a,l+1,r)
            a[l],a[i]=a[i],a[l]# backtrack

string="123"
n=len(string)
a=list(string)
permute(a,0,n-1)

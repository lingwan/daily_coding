# Given a list of integers and a number K, return which contiguous elements of the list sum to K.
# For example, if the list is [1, 2, 3, 4, 5] and K is 9, then it should return [2, 3, 4], since 2 + 3 + 4 = 9.

# Choose a particular start point and while iterating over the end points, add the element corresponding to the end point
# to the sum formed until now. 
def subarraySum(nums,k):
    for start in range(len(nums)):
        sum=0
        for end in range(start,len(nums)):
            sum+=nums[end]
            if sum==k:
                return nums[start:end+1]

print(subarraySum([1, 2, 3, 4, 5],9))
print(subarraySum([1,3,5,7],8))
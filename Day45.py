# There is an N by M matrix of zeroes. Given N and M, write a function to count the number of ways of starting at the
# top-left corner and getting to the bottom-right corner. You can only move right or down.

# 1: O(N * M) time and space
# def num_ways(n,m):
#     A=[[0 for __ in range(m)] for __ in range(n)]
#     for i in range(n):
#         A[i][0]=1
#     for j in range(m):
#         A[0][j]=1
#     for i in range(1,n):
#         for j in range(1,m):
#             A[i][j]=A[i-1][j]+A[i][j-1]
#     return A[-1][-1]

# 2: recursive solution
def num_ways(n,m):
    if n==1 or m==1:
        return 1
    return num_ways(n-1,m)+num_ways(n,m-1)

print(num_ways(5,5));
# Given an array of strictly the characters 'R', 'G', and 'B', segregate the values of the array so that all the Rs come
# first, the Gs come second, and the Bs come last. You can only swap elements of the array.
# [hard]

def partition(arr):
    low, mid, high = 0, 0, len(arr) - 1
    while mid <= high:
        if arr[mid] == 'R':
            arr[low], arr[mid] = arr[mid], arr[low]
            low += 1
            mid += 1
        elif arr[mid] == 'G':
            mid += 1
        else:
            arr[mid], arr[high] = arr[high], arr[mid]
            high -= 1
    arr=[]
    for i in range(low):
        arr.append('R')
    for i in range(low):
        arr.append('G')
    for i in range(low):
        arr.append('B')
    return arr

print(partition(['G', 'B', 'R', 'R', 'B', 'R', 'G']))
# The power set of a set is the set of all its subsets. Write a function that, given a set, generates its power set.
# runtime :O(2^N) [easy]
def power_set(s):
    if not s:
        return [[]]
    result = power_set(s[1:])
    return result + [subset + [s[0]] for subset in result]

print(power_set([1,2,3]))
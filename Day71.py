# Given a 2D board of characters and a word, find if the word exists in the grid.
#
# The word can be constructed from letters of sequentially adjacent cell, where "adjacent" cells are those horizontally
# or vertically neighboring. The same letter cell may not be used more than once.

# For example, given the following board:
#
# [
#   ['A','B','C','E'],
#   ['S','F','C','S'],
#   ['A','D','E','E']
# ]
# exists(board, "ABCCED") returns true, exists(board, "SEE") returns true, exists(board, "ABCB") returns false.
# [medium] A typical DFS algorithm

def exist(board,word):
    m=len(board)
    n=len(board[0])

    result=False
    for i in range(m):
        for j in range(n):
            if (dfs(board,word,i,j,0)):
                result=True
    return result

def dfs(board,word,i,j,k):
    m = len(board)
    n = len(board[0])

    if i<0 or j<0 or i>=m or j>=n:
        return False

    if board[i][j]==word[k]:
        temp=board[i][j]
        board[i][j]='#'

        if k==len(word)-1:
            board[i][j] = temp
            return True
        elif dfs(board,word,i-1,j,k+1) or dfs(board,word,i+1,j,k+1) \
            or dfs(board,word,i,j-1,k+1) or dfs(board,word,i,j+1,k+1):
            board[i][j] = temp
            return True

    return False

board=[
  ['A','B','C','E'],
  ['S','F','C','S'],
  ['A','D','E','E']
]
print(exist(board, "ABCCED"))
print(exist(board, "SEE"))
print(exist(board, "ABCB"))

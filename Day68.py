# Given a binary tree of integers, find the maximum path sum between two nodes.
# The path must go through at least one node, and does not need to go through the root.

def maxPathSum(root,res):
    # Base case
    if root is None:
        return 0

    if root.left is None and root.right is None:
        return root.data

    # Find maximum sum in left and right subtree. Also
    # find maximum root to leaf sums in left and right
    # subtrees and store them in ls and rs
    ls=maxPathSum(root.left,res)
    rs=maxPathSum(root.right,res)

    # If both left and right children exist
    if root.left is not None and root.right is not None:
        # update result if needed
        res=max(res[0],ls+rs+root.data)

        # Return maximum possible value for root bein gon one side
        return max(ls,rs)+root.data

    # If any of the two children is empty, return root sum for root being on one side
    if root.left is None:
        return rs+root.data
    else:
        return ls+root.data

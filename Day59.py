# Invert a binary tree.
# For example, given the following tree:
#
#     a
#    / \
#   b   c
#  / \  /
# d   e f
# should become:
#
#   a
#  / \
#  c  b
#  \  / \
#   f e  d


def invertTree(self,root):
    if root is not None:
        root.left,root.right=self.invertTree(root.right),self.invertTree(root.left)
    return root

# Solution:
def invert(node):
    if not node:
        return node

    left=invert(node.left)
    right=invert(node.right)

    node.left,node.right=right,left
    return node
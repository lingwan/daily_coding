# Given a list of integers, return the largest product that can be made by multiplying any three integers.

def maxProduct(array):
    n=len(array)
    array.sort()
    return max(array[0]*array[1]*array[n-1],array[n-1]*array[n-2]*array[n-3])

print(maxProduct([-10, -10, 5, 2]))

# solution:
# O(N log N) 
def maximum_product_of_three(lst):
    lst.sort()
    third_largest, second_largest, first_largest = lst[-3], lst[-2], lst[-1]
    first_smallest, second_smallest = lst[0], lst[1]
    return max(third_largest * second_largest * first_largest,
    first_largest * first_smallest * second_smallest)
# Given tow singly linked lists that intersect at some point, find the intersecting node.
# eg: given A=3->7->8->10 and B=99->1->8->10, return the node with value 8.

# Get the length of both lists. Find the difference between the two and keep two pointers at the head of each list.
# Move the pointer of the larger list up by the difference and then move the pointers forward in conjunction until
# they match. 

def length(head):
    if not head:
        return 0
    return 1+length(head.next)

def intersection(a,b):
    m,n=length(a),length(b)
    cur_a,cur_b=a,b

    if m>n:
        for _ in range(m-n):
            cur_a=cur_a.next
    else:
        for _ in range(n-m):
            cur_b=cur_b.next

    while cur_a!=cur_b:
        cur_a=cur_a.next
        cur_b=cur_b.next

    return cur_a
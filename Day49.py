# Given an array of integers that are out of order, determine the bounds of the smallest window that must be sorted
# in order for the entire array to be sorted
# O(nlogn)

def window(array):
    left, right=None, None
    s=sorted(array)

    for i in range(len(array)):
        if array[i]!=s[i] and left is None:
            left=i
        elif array[i]!=s[i]:
            right=i
    return left,right

print(window([3,7,5,6,9]))
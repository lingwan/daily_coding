# Given an unsorted array of integers, find the length of the longest consecutive elements sequence.
# For example, given [100, 4, 200, 1, 3, 2], the longest consecutive element sequence is [1, 2, 3, 4]. Return its length: 4.

def longestConsecutive(arr):
    longest_streak=0

    for num in arr:
        if num-1 not in arr:
            current_num=num
            current_streak=1

            while current_num+1 in arr:
                current_num+=1
                current_streak+= 1

            current_streak=max(longest_streak,current_streak)

    return current_streak

print(longestConsecutive([100, 4, 200, 1, 3, 2]))


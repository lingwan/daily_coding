# A number is considered perfect if its digits sum up to exactly 10.
#
# Given a positive integer n, return the n-th perfect number.

def perfectNum(n):
    original=n
    total=0
    while n>0:
        digit=n%10
        total+=digit
        n=n//10
    return original*10+10-total

print(perfectNum(20))


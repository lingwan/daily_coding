# Given the head of a singly linked list, reverse it in-place.
# [easy]
# Solution 1: iteratively
def reserse(head):
    prev,current=None,head
    while current is not None:
        tmp=current.next
        current.next=prev
        prev=current
        current=tmp
    return prev

# Solution 2: recursively
def reserve(head,prev=None):
    if not head:
        return prev
    tmp=head.next
    head.next=prev
    return reserve(tmp,head)
# Given the root of a binary tree, return a deepest node
# Solution 1: O(n)
# maxLevel : keeps track of maximum level seen so far.
# res : Value of deepest node so far.
# level : Level of root

def find(root,level,maxLevel,res):
    if root!=None:
        level+=1
        find(root.left,level,maxLevel,res)

        if level>maxLevel:
            res[0]=root.data
            maxLevel[0]=level

        find(root.right,level,maxLevel,res)

def deepestNode(root):
    res=[-1]
    maxLevel=[-1]
    find(root,0,maxLevel,res)
    return res[0]

# Solution 2:
def deepest(node):
    if node and not node.left and not node.right:
        return (node,1)# leaf and its depth
    if not node.left:
        return increment_depth(deepest(node.right))
    elif not node.right:
        return increment_depth(deepest(node.left))

    # Pick higher depth tuple and then increment its depth
    return increment_depth(max(deepest(node.left),deepest(node.right),
                               key=lambda x:x[1]))

def increment_depth(node_depth_tuple):
    node,depth=node_depth_tuple
    return (node,depth+1)

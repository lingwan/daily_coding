# Given k sorted singly linked lists, write a function to merge all the lists into one sorted singly linked list.

def merge(lists):
    arr=[]
    for head in lists:
        current=head
        while current:
            arr.append(current.val)
            current=current.next

    new_head=current=Node(-1)
    for val in sorted(arr):
        current.next=Node(val)
        current=current.next

    return new_head.next
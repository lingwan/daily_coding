# Given a array of numbers representing the stock prices of a company in chronological order, write a function that calculates the maximum profit you could have made from buying and selling that stock once. You must buy before you can sell it.

def stock(price):
    buyPrice=price[0]

    for i in range(len(price)-1):
        if price[i]<buyPrice:
            buyPrice=price[i]
            buy=i
    j=buy+1
    sellPrice=price[j]
    while j<len(price):
        if price[j]>sellPrice:
            sellPrice=price[j]
        j+=1
    return sellPrice-buyPrice

print(stock([9, 11, 8, 5, 7, 18]))

# solution

def buy_and_sell(arr):
    current_max, max_profit = 0, 0
    for price in reversed(arr):
        current_max = max(current_max, price)
        potential_profit = current_max - price
        max_profit = max(max_profit, potential_profit)
    return max_profit
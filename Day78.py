# Reconstruct array using +/- signs
# eg: given [None,+,-,-,-] we can reverse the last three entries of [0,1,2,3,4] to get [0,1,4,3,2]

# For a run of positive signs, keep the elements from the original sequence. For a run of negative signs, push those
# elements onto the stack. When the run of negative ends, pop those elements off one by one to get a decreasing subsequence

def reconstruct(array):
    answer=[]
    n=len(array)-1
    stack=[]

    for i in range(n):
        if array[i]=='-':
            stack.append(i)
        else:
            answer.append(i)
            while stack:
                answer.append(stack.pop())

    stack.append(n)
    while stack:
        answer.append(stack.pop())

    return answer

print(reconstruct([None,'+','-','-','-']))
print(reconstruct([None,'-','+','-','+']))
# Given an array of integers, return a new array where each element in the new array is the number of smaller elements
# to the right of that element in the original input array
# O(n*n) time

def smaller_counts(lst):
    result=[]
    for i,num in enumerate(lst):
        count=sum(val < num for val in lst[i+1:])
        result.append(count)
    return result

print(smaller_counts([3,4,9,6,1]))
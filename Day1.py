#Given a list of numbers and a number k, return whether any two numbers from the list add up to k.

def two_sum(lst,k):
    seen=set()
    for num in lst:
        if k-num in seen:
            return True
        seen.add(num)
    return False


lst=[10, 15, 3, 7]
k=17
print(two_sum(lst,k))
# Given a list of words, find all pairs of unique indices such that the concatenation of two words is a palindrome
# eg: given the list ["code","edoc","da","d"], return [(0,1),(1,0),(2,3)]

def is_palindrome(word):
    return word==word[::-1]

def palindrome_pairs(words):
    result=[]

    for i,word1 in enumerate(words):
        for j,word2 in enumerate(words):
            if i==j:
                continue
            if is_palindrome(word1+word2):
                result.append((i,j))
    return result

print(palindrome_pairs(["code","edoc","da","d"]))
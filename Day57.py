# Given a mapping of digits to letters (as in a phone number), and a digit string, return all possible letters the number could represent. You can assume each valid number in the mapping is a single digit.
# this is a solution for two digits. Three digits?

def digitsToLetters(dict,digits):
    result= []
    for x in dict.get(digits[0]):
        for y in dict.get(digits[1]):
            for z in dict.get(digits[2]):
                result.append(x+y+z)
    return result


# print(digitsToLetters({"2": ["a", "b", "c"], "3": ["d", "e", "f"],"4":["g","h"],"5":["j","k"]},"234"))


# Solution:recursive
def get_permutations(digits,mapping):
    digit=digits[0]

    if len(digits)==1:
        return mapping[digit]

    result=[]
    for char in mapping[digit]:
        for perm in get_permutations(digits[1:],mapping):
            result.append(char+perm)
    return result

print(get_permutations("234",{"2": ["a", "b", "c"], "3": ["d", "e", "f"],"4":["g","h"],"5":["j","k"]}))

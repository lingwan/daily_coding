# Given a 2D matrix of characters and a target word, write a function that returns whether the word can be found in the
# matrix by going left-to-right, or up-to-down.
A = [['F', 'A', 'C', 'I'],
     ['O', 'B', 'Q', 'P'],
     ['A', 'N', 'O', 'B'],
     ['M', 'A', 'S', 'S']]

def foundInMatrix(target):

    leftToRight=""
    upToDown=""

    for i in range(len(A)):
        for j in range(len(A[i])):
            leftToRight+=A[i][j]
        leftToRight+="\\"
    if leftToRight.find(target)!=-1:
        return True

    for i in range(len(A[0])):
        for j in range(len(A)):
            upToDown+=A[j][i]
        upToDown+="\\"
    if upToDown.find(target)!=-1:
        return True
    return False

# Solution:
def build_word_right(matrix, r, c, length):
    row_len = len(matrix[0])
    return ''.join([matrix[r][i] for i in range(c, min(row_len, length))])

def build_word_down(matrix, r, c, length):
    col_len = len(matrix)
    return ''.join([matrix[i][c] for i in range(r, min(col_len, length))])

def word_search(matrix, word):
    for r in range(len(matrix)):
        for c in range(len(matrix[0])):
            word_right = build_word_right(matrix, r, c, len(word))
            word_down = build_word_down(matrix, r, c, len(word))
            if word in (word_right, word_down):
                return True
    return False

# print(foundInMatrix("FOAM"))
print(word_search(A,"FOAM"))
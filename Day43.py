# Given a multiset of integers, return whether it can be partitioned into two subsets whose sums are the same.

def power_set(s):
    if not s:
        return [[]]
    result = power_set(s[1:])
    return result + [subset + [s[0]] for subset in result]

def partition(s):
    k = sum(s)
    if k % 2 != 0:
        return False
    powerset = power_set(s)
    for subset in powerset:
        if sum(subset) == k / 2:
            return True
    return False

print(partition([15, 5, 20, 10, 35, 15, 10]))
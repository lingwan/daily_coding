# Given a binary tree, return all paths from the root to leaves.
#
# For example, given the tree:
#
#    1
#   / \
#  2   3
#     / \
#    4   5
# Return [[1, 2], [1, 3, 4], [1, 3, 5]].

class Node:
    # constructor to create tree node
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

def printPaths(root):
    path=[]
    printPathsRec(root,path,0)

def printPathsRec(root,path,pathLen):
    # if binary tree is empty
    if root is None:
        return

    # add current root data into path list
    if (len(path)>pathLen):
        path[pathLen]=root.data
    else:
        path.append(root.data)

    pathLen+=1

    if root.left is None and root.right is None:
        # leaf node
        printArray(path,pathLen)
    else:
        printPathsRec(root.left,path,pathLen)
        printPathsRec(root.right,path,pathLen)

def printArray(array,len):
    for i in array:
        print(i," ",end="")
    print()

root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.right.left = Node(4)
root.right.right = Node(5)

printPaths(root)
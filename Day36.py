# Given pre-order and in-order traversals of a binary tree, write a function to reconstruct the tree.

def reconstruct(preorder, inorder):
    if not preorder and not inorder:
        return None
    if len(preorder) == len(inorder) == 1:
        return preorder[0]

    root = preorder[0]
    root_i = inorder.index(root)
    root.left = reconstruct(preorder[1: 1+root_i], inorder[0:root_i])
    root.right = reconstruct(preorder[1 + root_i:], inorder[root_i + 1:])
    return root

def test(order):
    print(order[1:4])
print(test([1,2,3,4,5,6,7]))